import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Reuse_case/block_login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Reuse_case/block_cart'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Bayar/Cart/btn_agree'), 0)

WebUI.click(findTestObject('Bayar/Cart/btn_agree'))

WebUI.click(findTestObject('Bayar/Cart/btn_Checkout'))

WebUI.delay(0)

WebUI.click(findTestObject('Bayar/Checkout/btn_cntnuBill'))

WebUI.waitForElementPresent(findTestObject('Bayar/Checkout/ShipAddrs'), 0)

WebUI.click(findTestObject('Bayar/Checkout/btn_cntnuShip'))

WebUI.waitForElementPresent(findTestObject('Bayar/Checkout/Method'), 0)

WebUI.click(findTestObject('Bayar/Checkout/btn_cntnuMettd'))

WebUI.waitForElementPresent(findTestObject('Bayar/Checkout/payMettd'), 0)

WebUI.click(findTestObject('Bayar/Checkout/btn_cntnuPaymettd'))

WebUI.waitForElementPresent(findTestObject('Bayar/Checkout/PayInfo'), 0)

WebUI.click(findTestObject('Bayar/Checkout/bnt_cntnuPayinfo'))

WebUI.verifyElementPresent(findTestObject('Bayar/Checkout/ConfirmOrder'), 0)

WebUI.click(findTestObject('Bayar/Checkout/btn_cnfrm'))

WebUI.waitForElementPresent(findTestObject('Object Repository/Bayar/Checkout/def_orderSucses'), 0)

WebUI.verifyElementText(findTestObject('Object Repository/Bayar/Checkout/def_orderSucses'), 'Your order has been successfully processed!')

WebUI.click(findTestObject('Object Repository/Bayar/Checkout/bnt_orderDetail'))

WebUI.waitForElementPresent(findTestObject('Bayar/Checkout/OrderInfo'), 0)

