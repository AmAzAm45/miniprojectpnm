import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.waitForElementVisible(findTestObject('Search Box/searchBox'), 0)

for (def row = 1; row <= findTestData('Item_cart').getRowNumbers(); row++) {
    WebUI.setText(findTestObject('Search Box/searchBox'), findTestData('Item_cart').getValue('item_name', row))

    obj = WebUI.getText(findTestObject('Search Box/searchBox'))

    WebUI.click(findTestObject('Search Box/btn_search'))

    WebUI.waitForElementVisible(findTestObject('Search Box/obj_name'), 0)

    objSearch = WebUI.getText(findTestObject('Search Box/obj_name'))

    def obj = objSearch

    WebUI.verifyElementVisible(findTestObject('Search Box/obj_searchs'), FailureHandling.STOP_ON_FAILURE)

    objfound = WebUI.getText(findTestObject('Search Box/obj_searchs'))

    if (objfound.contains(obj)) {
        println('ok.')
    } else {
        WebUI.closeBrowser()
    }
    
    WebUI.click(findTestObject('Cart/Page_Demo Web Shop/btn_add to cart'))

    WebUI.verifyElementPresent(findTestObject('Cart/alert/div_succes'), 0)
}

WebUI.click(findTestObject('Cart/Page_Demo Web Shop/btn_cart'))

WebUI.verifyElementVisible(findTestObject('Cart/Page_Demo Web Shop/Produk_info'))

