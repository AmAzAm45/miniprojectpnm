<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OrderInfo</name>
   <tag></tag>
   <elementGuidId>24f4e650-2c37-4eef-9a4c-5a7571f943b3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.product-price.order-total > strong</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[4]/div[1]/div[4]/div/div/div[1]/h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>2667e271-642b-45f1-949c-846feb9694dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>164.00</value>
      <webElementGuid>17313cb7-6939-47cd-9731-3e05e76db1b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/div[@class=&quot;cart-footer&quot;]/div[@class=&quot;totals&quot;]/div[@class=&quot;total-info&quot;]/table[@class=&quot;cart-total&quot;]/tbody[1]/tr[5]/td[@class=&quot;cart-total-right&quot;]/span[@class=&quot;nobr&quot;]/span[@class=&quot;product-price order-total&quot;]/strong[1]</value>
      <webElementGuid>58206053-1a21-4af6-8414-7a74388d736a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/form/div[2]/div[2]/div/table/tbody/tr[5]/td[2]/span/span/strong</value>
      <webElementGuid>c63c60f1-07be-4613-8ec1-cd3c28332c64</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total:'])[5]/following::strong[1]</value>
      <webElementGuid>53475838-a91c-4a0b-a1de-8ba0f44e19e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tax:'])[1]/following::strong[1]</value>
      <webElementGuid>04cd37e3-d6f7-4bd9-a15b-284ab77fe71a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='«'])[5]/preceding::strong[1]</value>
      <webElementGuid>12794f0b-8418-42f9-8c94-a0b4b171c936</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Submitting order information...'])[1]/preceding::strong[1]</value>
      <webElementGuid>82d0355a-f97e-4e75-b1c3-7821424ffb1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='164.00']/parent::*</value>
      <webElementGuid>78630ae7-a08b-42df-9aff-a65ff113373d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/strong</value>
      <webElementGuid>0dca3699-d76a-413d-9460-5a8a2dd2d0c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = '164.00' or . = '164.00')]</value>
      <webElementGuid>19e533b8-e06a-46ec-b74b-92da15ee8193</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
