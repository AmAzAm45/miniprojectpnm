<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_ReturningCustomer</name>
   <tag></tag>
   <elementGuidId>b3a0c7e3-1248-4795-988f-aa86e6526cc1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.returning-wrapper > div.title > strong</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='New Customer'])[1]/following::strong[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>e2a70c22-de73-4a2c-ab3a-8047e8cee5ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Returning Customer</value>
      <webElementGuid>057261c8-c31a-479a-b790-d7d89d27e500</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page login-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;customer-blocks&quot;]/div[@class=&quot;returning-wrapper&quot;]/div[@class=&quot;title&quot;]/strong[1]</value>
      <webElementGuid>6ea8a359-3573-4e97-b0c7-343d894bf753</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Customer'])[1]/following::strong[1]</value>
      <webElementGuid>74b3c2a6-6e33-496b-bf81-b5f336eddd77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome, Please Sign In!'])[1]/following::strong[2]</value>
      <webElementGuid>aac2a34e-da26-487b-9c96-924d66234c82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email:'])[1]/preceding::strong[1]</value>
      <webElementGuid>4c414535-a20d-4451-a03e-682e52a96283</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password:'])[1]/preceding::strong[1]</value>
      <webElementGuid>67bd0bb5-673b-459a-94a8-f05ac3546df3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Returning Customer']/parent::*</value>
      <webElementGuid>cc136cbc-036d-45b4-ad26-329d183d95dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/strong</value>
      <webElementGuid>b217f22b-ce01-409a-b6eb-9096b072909d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'Returning Customer' or . = 'Returning Customer')]</value>
      <webElementGuid>8081bc4d-7b6f-4c5e-b6b2-0e9557982132</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
