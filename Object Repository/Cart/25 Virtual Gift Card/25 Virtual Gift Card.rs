<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>25 Virtual Gift Card</name>
   <tag></tag>
   <elementGuidId>46d93386-e73b-435e-9e82-3127657ef44f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='/'])[2]/following::strong[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>strong.current-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>8c4c5ce7-9035-4dfa-9d6d-39832af582f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>current-item</value>
      <webElementGuid>6b263a34-a3a7-49f9-af4f-13d758239131</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>$25 Virtual Gift Card</value>
      <webElementGuid>e4ea3bfb-1ca0-431a-a578-6816797a5492</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;breadcrumb&quot;]/ul[1]/li[3]/strong[@class=&quot;current-item&quot;]</value>
      <webElementGuid>c680af57-f893-4abb-9df8-dc4358789249</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='/'])[2]/following::strong[1]</value>
      <webElementGuid>26d718c3-7c4a-465d-b35e-43ea3090268e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gift Cards'])[4]/following::strong[1]</value>
      <webElementGuid>71a98371-c923-4048-bc12-694bf2c31957</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$25 Virtual Gift Card'])[2]/preceding::strong[1]</value>
      <webElementGuid>2aa53687-86f6-40cf-b247-357b01e884b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='|'])[1]/preceding::strong[1]</value>
      <webElementGuid>3076b2cf-b0f5-4693-9290-c2134f8441a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='$25 Virtual Gift Card']/parent::*</value>
      <webElementGuid>475f01a8-aa93-425d-b27f-d11b41d8005a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/strong</value>
      <webElementGuid>046d030e-3d54-4d92-b7fb-dfc0dc179557</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = '$25 Virtual Gift Card' or . = '$25 Virtual Gift Card')]</value>
      <webElementGuid>9646a5c5-f8fc-4c76-8dc8-bf9142fd8403</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
