<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FeaturedProduct</name>
   <tag></tag>
   <elementGuidId>989d4163-4d13-4684-9dbf-f532a774e5af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome to our store'])[1]/following::strong[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.product-grid.home-page-product-grid > div.title > strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>9739219f-53fe-4deb-878b-35706657f6e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Featured products</value>
      <webElementGuid>259d90c4-7c38-4030-8fd0-3024242d7946</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-3&quot;]/div[@class=&quot;page home-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;product-grid home-page-product-grid&quot;]/div[@class=&quot;title&quot;]/strong[1]</value>
      <webElementGuid>de82c1da-a16d-49e2-9fc8-93c5dbddbf46</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome to our store'])[1]/following::strong[1]</value>
      <webElementGuid>840c2d62-9c02-4d17-8637-540ba571cbc6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::strong[1]</value>
      <webElementGuid>19eadd33-b122-4a36-bc94-d0b1b34cfc55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$25 Virtual Gift Card'])[1]/preceding::strong[1]</value>
      <webElementGuid>bcc4ac3d-55e4-4021-b1c4-57147b4a2b68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unique Asian-influenced imprint wraps the laptop both inside and out'])[1]/preceding::strong[1]</value>
      <webElementGuid>a8cc74f8-2da9-43cb-a671-e404dc8332ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Featured products']/parent::*</value>
      <webElementGuid>174cac0a-dc3e-4d56-9f2f-536978e942a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div[3]/div/strong</value>
      <webElementGuid>e354b955-b8f3-4292-99ac-c1888d32cdad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'Featured products' or . = 'Featured products')]</value>
      <webElementGuid>5334fa6f-646f-403d-aa09-6fa25690e177</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
