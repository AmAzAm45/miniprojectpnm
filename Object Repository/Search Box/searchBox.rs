<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>searchBox</name>
   <tag></tag>
   <elementGuidId>ac0ebc57-b3b5-4263-8b50-1bc7329a1476</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#small-searchterms</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='small-searchterms']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>6cb7a380-61ba-4ed5-b601-e29ac1eb4e2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>974651ab-6f87-4966-96aa-c6a8c85a2f2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>search-box-text ui-autocomplete-input</value>
      <webElementGuid>c86a30f6-eb10-403a-a1e3-672f9cb2fd7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>small-searchterms</value>
      <webElementGuid>88342d14-e12a-4405-9f79-01fc198e7276</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>ab9fa74a-6c31-4777-ba0a-b61b6c98c1c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Search store</value>
      <webElementGuid>81222388-2060-4ac2-a5da-ea2752e9e430</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>q</value>
      <webElementGuid>bdca5981-4b73-4298-b4a3-d22818f63235</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;small-searchterms&quot;)</value>
      <webElementGuid>62accf80-868b-418a-b715-a90bd9d5c74b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='small-searchterms']</value>
      <webElementGuid>2f080519-79a1-44ba-a4ba-29514f30f970</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>d7ef1f79-de9a-4003-9068-5eda7337fd05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @id = 'small-searchterms' and @name = 'q']</value>
      <webElementGuid>c3c55e7e-4c87-43b6-9204-236aeef03b3b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
