<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_search min 3 char</name>
   <tag></tag>
   <elementGuidId>0029a327-f046-4ec4-b6d2-2ef532a73f12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>strong.warning</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Search In product descriptions'])[1]/following::strong[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>de8ba0e4-c609-4260-8339-25d111c7325a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>warning</value>
      <webElementGuid>e8286d8d-0e60-4a89-a5b3-bafd705ff7f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Search term minimum length is 3 characters
                </value>
      <webElementGuid>efebc5f7-e172-4fb3-96cf-fd8bf81271e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-results&quot;]/strong[@class=&quot;warning&quot;]</value>
      <webElementGuid>e1e59f32-0333-404a-b5a0-cbdb315b5517</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search In product descriptions'])[1]/following::strong[1]</value>
      <webElementGuid>f9eba546-b90a-4048-8bd9-158f25d8b7fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::strong[1]</value>
      <webElementGuid>d828915d-6caa-4a4b-a704-cb980528c0a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sitemap'])[1]/preceding::strong[1]</value>
      <webElementGuid>143fb8fb-72b1-468f-ba57-74042ef3583f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Search term minimum length is 3 characters']/parent::*</value>
      <webElementGuid>9dfc6bf6-e8f8-4c65-8e66-7c96cb2e5509</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/strong</value>
      <webElementGuid>f2f08d73-eaa9-4720-8322-1a8b66b97500</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = '
                    Search term minimum length is 3 characters
                ' or . = '
                    Search term minimum length is 3 characters
                ')]</value>
      <webElementGuid>21155134-bada-4b22-a853-fb3ab7b6e80e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
