<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_search</name>
   <tag></tag>
   <elementGuidId>ac50927d-0a00-4f1c-b29c-3988e868d8a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.button-1.search-box-button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='Search']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ca55afa2-132d-4140-8e2e-42fff1599c94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>fa212f7a-b975-4f18-9681-28d8cf7f201f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-1 search-box-button</value>
      <webElementGuid>fb737bb1-918e-422b-9433-0e9414c6e178</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Search</value>
      <webElementGuid>82449322-5374-4e9a-a08d-82f82ad4ace3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;header&quot;]/div[@class=&quot;search-box&quot;]/form[1]/input[@class=&quot;button-1 search-box-button&quot;]</value>
      <webElementGuid>f2924781-9698-4251-8114-89918c335864</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='Search']</value>
      <webElementGuid>0cbd0c13-bdd1-4ac0-97ec-e641cbe4a71e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input[2]</value>
      <webElementGuid>c1146033-0ff0-4d6f-aacd-0ee283884147</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit']</value>
      <webElementGuid>c077745e-da24-4f8c-9733-f487b082969c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
