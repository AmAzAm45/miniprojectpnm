<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>inpt_Password</name>
   <tag></tag>
   <elementGuidId>fcfd8f76-bdcc-4753-b056-2850546b01ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#Password</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>9239f566-6dee-4fee-854a-6d897f51ccdb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-box single-line password</value>
      <webElementGuid>e66112fd-c45b-472b-8635-18c9f8814283</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>62dec806-3bae-43c8-a539-041bcd40a1b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-length</name>
      <type>Main</type>
      <value>The password should have at least 6 characters.</value>
      <webElementGuid>4a211b06-a31f-4a47-a5ba-88233cd2bc00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-length-max</name>
      <type>Main</type>
      <value>999</value>
      <webElementGuid>5761d1ab-0991-4255-9467-082feee13d9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-length-min</name>
      <type>Main</type>
      <value>6</value>
      <webElementGuid>da934ba4-9189-4f56-9b40-097b4518ea3b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-required</name>
      <type>Main</type>
      <value>Password is required.</value>
      <webElementGuid>1226a1ea-dd33-44b8-ae5e-f41d994735f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Password</value>
      <webElementGuid>030b14d6-198c-403e-9ec7-f784ab0ea59e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Password</value>
      <webElementGuid>ac667c4a-ae5f-4b63-9c33-dd8220414c36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>1e3df31a-db1a-4c14-96f0-78577a1aa627</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Password&quot;)</value>
      <webElementGuid>ac9bde7f-775c-41fd-a84f-56d3ba593054</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Password']</value>
      <webElementGuid>70df76a0-4d23-4679-ba70-3cfbf498f6b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div/input</value>
      <webElementGuid>340cff38-2d18-45ab-bb9b-ddc6298e6543</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Password' and @name = 'Password' and @type = 'password']</value>
      <webElementGuid>68f8f409-e10e-4472-a6a9-2a6e5c69198c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
